//
//  ActivityDetailViewController.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 17/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit
import MapKit

class ActivityDetailViewController: UIViewController {

    @IBOutlet weak var activityImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var activityCategoryLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    
    var viewModel =  ActivityDetailViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        displayData()

        setupMap()
    }
}

extension ActivityDetailViewController : ActivityDetailViewDelegate {

    func displayData() {
        activityImageView.imageFromServerURL(urlString: viewModel.activitySelected.imageURL, defaultImage: "")
        descriptionLabel.text = viewModel.activitySelected.description
        activityCategoryLabel.text = viewModel.activitySelected.category
    }

    func setupMap() {
        let location = CLLocationCoordinate2D(latitude: viewModel.activitySelected.address.latitude,longitude: viewModel.activitySelected.address.longitude)

        let span = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region = MKCoordinateRegion(center: location, span: span)
        mapView.setRegion(region, animated: true)

        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        annotation.title = viewModel.activitySelected.name
        annotation.subtitle = viewModel.activitySelected.placeName
        mapView.addAnnotation(annotation)
    }
}
