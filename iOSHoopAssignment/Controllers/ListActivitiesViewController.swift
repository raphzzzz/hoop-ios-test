//
//  ViewController.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit

class ListActivitiesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var layerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var errorStackView: UIStackView!
    
    private var viewModel: ListActivitiesViewModel!

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = ListActivitiesViewModel(delegate: self)

        setupTableView()

        viewModel.loadActivities()
    }

    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: String(describing: ActivityTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: ActivityTableViewCell.self))

        tableView.separatorStyle = .none

        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        tableView.addSubview(refreshControl)
    }

    @IBAction func retryHandler(_ sender: Any) {
        viewModel.loadActivities()
    }

    @objc func refresh(sender:AnyObject) {
        viewModel.loadActivities()
    }
}

extension ListActivitiesViewController : ListActivitiesViewDelegate {

    func loadData() {
        stopLoading()
        tableView.isHidden = false
        tableView.reloadData()
        refreshControl.endRefreshing()
    }

    func startLoading() {
        tableView.isHidden = true
        layerView.isHidden = false
        errorStackView.isHidden = true
        activityIndicator.isHidden = false
    }

    func stopLoading() {
        layerView.isHidden = true
        activityIndicator.isHidden = true
        errorStackView.isHidden = true
    }

    func showErrorView() {
        layerView.isHidden = false
        tableView.isHidden = true
        errorStackView.isHidden = false
        activityIndicator.isHidden = true
    }
}

extension ListActivitiesViewController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "showActivityDetail" {
            let destinationVC = segue.destination as! ActivityDetailViewController
            destinationVC.viewModel.activitySelected = viewModel.activitySelected
        }
    }
}

extension ListActivitiesViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        viewModel.setSelectedActivity(index: indexPath.row)

        self.performSegue(withIdentifier: "showActivityDetail", sender: nil)
    }
}

extension ListActivitiesViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getActivitiesListSize()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ActivityTableViewCell.self)) as? ActivityTableViewCell else {
            fatalError("Couldn't dequeue ActivityTableViewCell")
        }

        cell.selectionStyle = .none

        let activity = viewModel.getActivityForIndex(index: indexPath.row)

        cell.activityNameLabel.text = activity.name
        cell.activityPlaceNameLabel.text = activity.placeName
        cell.activityAgeLabel.text = activity.ages
        cell.activityDateLabel.text = activity.startDate
        cell.activityThumbImageView.imageFromServerURL(urlString: activity.imageURL, defaultImage: "")

        return cell
    }
}
