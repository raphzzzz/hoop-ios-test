//
//  UIImageViewExtension.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func imageFromServerURL(urlString: String, defaultImage : String?) {

        self.layer.masksToBounds = true
        self.clipsToBounds = true

        if let di = defaultImage {
            self.image = UIImage(named: di)
        }

        URLSession.shared.dataTask(with: NSURL(string: urlString)! as URL, completionHandler: { (data, response, error) -> Void in

            if error != nil {
                print(error ?? "error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                self.image = image
            })

        }).resume()
    }
}
