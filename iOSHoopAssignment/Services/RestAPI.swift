//
//  RestAPI.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class RestAPI {

    private static let URL_ENDPOINT = URL(string: "http://files.hoop.co.uk/test.json")!

    func sendAPIRequest(completion: @escaping (Data?)->Void) {

        let url = RestAPI.URL_ENDPOINT

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completion(nil)
                return
            }

            completion(data)
        }

        task.resume()
    }
}
