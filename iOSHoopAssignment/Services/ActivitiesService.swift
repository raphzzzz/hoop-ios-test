//
//  ActivitiesService.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class ActivitiesService {

    func loadActivities(completion: @escaping ([Activity]?)->Void) {
        RestAPI().sendAPIRequest(completion: { data in

            if let responseData = data {
                let decoder = JSONDecoder()

                do {
                    let response = try decoder.decode([Activity].self, from: responseData)

                    completion(response)
                } catch {
                    completion(nil)
                }
            } else {
                completion(nil)
            }
        })
    }
}
