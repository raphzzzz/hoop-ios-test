//
//  ActivityDetailViewModel.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 17/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

protocol ActivityDetailViewDelegate: class {
    func displayData()
}

class ActivityDetailViewModel {

    weak var delegate: ActivityDetailViewDelegate!

    var activitySelected: Activity!
}
