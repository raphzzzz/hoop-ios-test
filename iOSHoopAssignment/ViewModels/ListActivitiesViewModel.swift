//
//  ListActivitiesViewModel.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

protocol ListActivitiesViewDelegate: class {
    func startLoading()
    func stopLoading()
    func showErrorView()
    func loadData()
}

class ListActivitiesViewModel {

    private let service = ActivitiesService()

    weak var delegate: ListActivitiesViewDelegate!

    private var activities: [Activity]!

    var activitySelected: Activity?

    init(delegate: ListActivitiesViewDelegate) {
        self.delegate = delegate
    }

    func loadActivities() {
        delegate.startLoading()

        service.loadActivities(completion: doneLoadingActivities)
    }

    func doneLoadingActivities(responseActivities: [Activity]?) {

        guard responseActivities != nil else {
            DispatchQueue.main.async {
                self.delegate.showErrorView()
            }
            return
        }

        setActivities(activities: responseActivities)

        DispatchQueue.main.async {
            self.delegate.loadData()
        }
    }

    func setActivities(activities: [Activity]!) {
        self.activities = activities
    }

    func getActivities() -> [Activity] {
        return self.activities
    }

    func getActivityForIndex(index: Int) -> Activity {
        return getActivities()[index]
    }

    func setSelectedActivity(index: Int) {
        activitySelected = getActivityForIndex(index: index)
    }

    func getActivitiesListSize() -> Int {
        if activities == nil {
            return 0
        } else {
            return activities.count
        }
    }
}
