//
//  Activity.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class Activity : Decodable {

    var ages: String
    var category: String
    var startDate: String
    var endDate: String
    var id: String
    var imageURL: String
    var name: String
    var placeName: String
    var description: String
    var address: Address

    enum ActivityKey: String, CodingKey {
        case ages
        case category
        case startDate
        case endDate
        case id
        case imageURL
        case name
        case placeName
        case description
        case address
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ActivityKey.self)
        //date = try container.decode(String.self, forKey: .date)
        ages = try container.decode(String.self, forKey: .ages)
        category = try container.decode(String.self, forKey: .category)
        id = try container.decode(String.self, forKey: .id)
        imageURL = try container.decode(String.self, forKey: .imageURL)
        name = try container.decode(String.self, forKey: .name)
        placeName = try container.decode(String.self, forKey: .placeName)
        description = try container.decode(String.self, forKey: .description)
        address = try container.decode(Address.self, forKey: .address)

        let startDateString = try container.decode(String.self, forKey: .startDate)
        let endDateString = try container.decode(String.self, forKey: .endDate)
        let dateFormatter = ISO8601DateFormatter()

        let start = dateFormatter.date(from: startDateString)
        let end = dateFormatter.date(from: endDateString)

        let dateToStringFormatter = DateFormatter()
        dateToStringFormatter.dateFormat = "MM/dd/yyyy h:mm"

        startDate = dateToStringFormatter.string(from: start ?? Date())
        endDate = dateToStringFormatter.string(from: end ?? Date())
    }
}

