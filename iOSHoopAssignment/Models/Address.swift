//
//  Address.swift
//  iOSHoopAssignment
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class Address: Codable {

    var country: String
    var latitude: Double
    var longitude: Double
    var postcode: String
    var streetName: String
    var town: String

    enum AdressKey: String, CodingKey {
        case country
        case latitude
        case longitude
        case postcode
        case streetName
        case town
    }
}
