//
//  iOSHoopAssignmentTests.swift
//  iOSHoopAssignmentTests
//
//  Created by Raphael Pedrini Velasqua on 16/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import XCTest
@testable import iOSHoopAssignment

class iOSHoopAssignmentTests: XCTestCase {

    let listActivitiesViewModel = ListActivitiesViewModel(delegate: ListActivitiesViewControllerMock())

    var activitiesMock: [Activity]?

    override func setUp() {
        let bundle = Bundle(for: iOSHoopAssignmentTests.self)

        if let path = bundle.url(forResource: "ActivitiesResponseMock", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                activitiesMock = try JSONDecoder().decode([Activity].self, from: data)
                listActivitiesViewModel.setActivities(activities: activitiesMock)
            } catch {
                XCTFail()
            }
        }
    }

    func testInit_initMock() {
        XCTAssertNotNil(ListActivitiesViewControllerMock())
    }

    func testInit_initViewModel() {
        XCTAssertNotNil(listActivitiesViewModel)
    }

    func testInit_GetActivities() {
        XCTAssertNotNil(listActivitiesViewModel.getActivities())
    }

    func testInit_loadActivitiesFromMockFile() {
        XCTAssertNotNil(activitiesMock)
    }

    func testInit_GetActivityByIndex() {
        XCTAssertNotNil(listActivitiesViewModel.getActivityForIndex(index: 0))

        let activity = activitiesMock?[0]

        if activity?.name == listActivitiesViewModel.getActivityForIndex(index: 0).name {
            XCTAssert(true)
        }else {
            XCTFail()
        }
    }

    func testInit_SetSelectedActivity() {

        let activity = listActivitiesViewModel.getActivityForIndex(index: 0)
        XCTAssertNotNil(activity)

        listActivitiesViewModel.setSelectedActivity(index: 0)

        if activity.name == listActivitiesViewModel.activitySelected?.name {
            XCTAssert(true)
        } else {
            XCTFail()
        }
    }

    func testInit_GetActivitiesCount() {
        XCTAssertNotNil(listActivitiesViewModel.getActivitiesListSize())
    }
}

class ListActivitiesViewControllerMock: ListActivitiesViewDelegate {
    func showErrorView() {}

    func startLoading() {}

    func stopLoading() {}

    func loadData() {}
}
