//
//  iOSHoopNetworkTests.swift
//  iOSHoopAssignmentTests
//
//  Created by Raphael Pedrini Velasqua on 17/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import XCTest

class iOSHoopNetworkTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testDownloadWebData() {

        let expectation = XCTestExpectation(description: "Reach tech test API")

        let url = URL(string: "http://files.hoop.co.uk/test.json")!

        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in

            XCTAssertNotNil(data, "No data was downloaded.")

            expectation.fulfill()

        }

        dataTask.resume()

        wait(for: [expectation], timeout: 10.0)
    }
}
